﻿#include <iostream>


template <typename T>
class Stack {

public:
    Stack(const int& = 5);
    ~Stack();
    bool push(const T);
    bool pop();
    void print();

private:
    T* stackPtr;
    int top;
    int _size;
};

int main()
{
    int stackSize;
    std::cout << "Write stack size:  ";
    std::cin >> stackSize;

    Stack <double> myStack(stackSize);

    std::cout << "\nWrite elements: \n";
    for (int num = 0; num < stackSize; ++num) {
        double temp;
        std::cin >> temp;
        myStack.push(temp);
    }

    myStack.print(); 

    
    std::cout << "\nRemove 1 element \n";
    myStack.pop(); 
    myStack.print(); 

    std::cout << "Remove 2 elements \n";
    myStack.pop(); 
    myStack.print();

    return 0;

}



template<typename T>
Stack<T>::Stack(const int& size)
{
    if (size > 0)
        _size = size;
    else
        _size = 5;

    stackPtr = new T[size];
    top = -1;

}

template<typename T>
Stack<T>::~Stack()
{
    delete [] stackPtr;
}

template<typename T>
bool Stack<T>::push(const T value)
{
    if (top == _size - 1)
        return false;

    top++;
    stackPtr[top] = value;

    return true;
}

template<typename T>
bool Stack<T>::pop()
{
    if (top == -1)
        return false; 

    stackPtr[top] = 0; 
    top--;

    return true;
}

template<typename T>
void Stack<T>::print()
{
    std::cout << "Stack stake: " << std::endl;
    for (int i = 0; i < _size; ++i)
        std::cout << i << ":  " << stackPtr[i] << std::endl;
    std::cout << std::endl;
}
